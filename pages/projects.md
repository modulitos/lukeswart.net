---
layout: page-fullwidth
title: Projects
# description: "Some of my projects"
comments: false
permalink: /projects/
---
<!-- --- -->
<!-- layout: blog -->
<!-- title: "Blogulus of Lucas" -->
<!-- teaser: "Homepage of Lucas Blogulus" -->
<!-- permalink: /blog/ -->
<!-- --- -->

### Hey Duwamish!

<img width="300" height="300" itemprop="image" src="/images/hey-duwamish_thumbnail.png" align="right" /> "Hey Duwamish!" is an environmental health monitoring platform to empower communities that are most affected by the toxic waste cleanup along Seattle's only river. There are dozens of stake holders associated with the area, and our goal is to "connect the dots" to encourage transparency and celebrate success for these programs.

Project page: [http://heyduwamish.org](http://heyduwamish.org)

Source Code: [https://github.com/mapseed/platform](https://github.com/mapseed/platform)

---

### The Duwamish Lighthouse

<img width="300" height="500" itemprop="image" src="/images/duwamish-lighthouse_facing-city.jpg" align="right" />The Duwamish Lighthouse responds to real-time water quality data and translates it into light, in the form of our own breathing: Steady and relaxed is a cleaner river, fast and spasmodic is a polluted river.

Project page: [http://www.georgeleestudio.com/duwamishlighthouse](http://www.georgeleestudio.com/duwamishlighthouse)

Source Code: [https://github.com/illumenati](https://github.com/illumenati)

---

### Untangler: A New Historical Analysis Tool with Fine-Grained History Framework
<!-- ![untangler snapshot][untangler] -->

<img width="300" height="500" itemprop="image" src="/images/untangler_screenshot_cropped.png" align="right" />Untangler is a tool that helps a developer organize changes during the commit phase. Unlike the staging area in a typical version control system, Untangler uses a fine-grained development history to help a developer cherry pick separate development tasks into different commits. Untangler uses the following hypothesis: when a developer is implementing a new feature or fixing a bug, most of the time the developer’s changes are localized to a few code elements (methods, classes, etc.). Therefore, Untangler creates a commit summary by transforming the fine-grained development history into a courser granularity (code-element-level granularity). My collaborators for this project were [Kivanc Muslu](http://www.kivancmuslu.com/index.html), [Michael D. Ernst](http://homes.cs.washington.edu/~mernst/), and [Yuriy Brun](http://people.cs.umass.edu/~brun/) in the UW Programming Languages and Software Engineering group.

My work was featured in a [publication](http://homes.cs.washington.edu/~mernst/pubs/history-transformations-ase2015.pdf) for the Automated Software Engineering conference.

Source code: [https://bitbucket.org/LukeSwart/untangler/overview/](https://bitbucket.org/LukeSwart/untangler/overview/)

---

### Solstice: Realtime Code Analysis through an IDE-Managed Code Copy
<!-- ![solstice logo][solstice] -->

<img width="200" height="200" itemprop="image" src="/images/solstice_logo.png" align="right" /> I assisted with the development of a replication framework, Solstice, that creates a copy codebase to run in parallel with development. This tool will allow developers to write and test their code at the same time. Working with the Eclipse API, my contribution to this project was to develop an abstraction that will extend the existing Solstice analyses wrappers. My advisor for this project was [Kivanc Muslu](http://www.kivancmuslu.com/index.html), in collaboration with [Michael D. Ernst](http://homes.cs.washington.edu/~mernst/), and [Yuriy Brun](http://people.cs.umass.edu/~brun/) in the UW Programming Language and Software Engineering group.

Source code: [https://bitbucket.org/kivancmuslu/solstice/wiki/Home](https://bitbucket.org/kivancmuslu/solstice/wiki/Home)

[untangler]: /images/untangler_screenshot_cropped.png "Untangler desktop image"
[solstice]: /images/solstice_logo.png "Solstice Logo"
