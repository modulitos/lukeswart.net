---
#
# Use the widgets beneath and the content will be
# inserted automagically in the webpage. To make
# this work, you have to use › layout: frontpage
#
layout: frontpage
header:
  # image_fullwidth: header_unsplash_12.jpg
widget1:
  title: "Take me to Lucas Blogulus!"
  url: "/blog/"
  image: sd-rr-tracks.jpg
  text: "Join me on my humble journey, doing my best to share what we know on the World Wide Web!"
widget2:
  title: "Projects"
  url: "/projects/"
  image: hey-duwamish_thumbnail.png
  text: "Here you'll find a list of my projects, ranging from polished apps to hair-brained adventures. Wanna join? I work with a diverse team, and we can use all kinds of help. Get in touch..."
widget3:
  title: "Get in touch!"
  url: "/contact/"
  image: with-bear.jpg
  text: "<p>Why can't we be friends</p><p>lswart on OFTC <i>(irc.oftc.net)</i><br>lswart on FreeNode <i>(irc.freenode.net)</i><br><a href='/keys/'>GPG Keys</a> for use in secure communication with me</p>"
#  <!-- video: '<a href="#" data-reveal-id="videoModal"><img src="http://phlow.github.io/feeling-responsive/images/start-video-feeling-responsive-302x182.jpg" width="302" height="182" alt=""/></a>' -->
# #
# # Use the call for action to show a button on the frontpage
# #
# # To make internal links, just use a permalink like this
# # url: /getting-started/
# #
# # To style the button in different colors, use no value
# # to use the main color or success, alert or secondary.
# # To change colors see sass/_01_settings_colors.scss
# #
# callforaction:
#   url: https://tinyletter.com/feeling-responsive
#   text: Inform me about new updates and features ›
#   style: alert

permalink: /index.html
#
# This is a nasty hack to make the navigation highlight
# this page as active in the topbar navigation
#
homepage: true
---
<div id="videoModal" class="reveal-modal large" data-reveal="">
  <div class="flex-video widescreen vimeo" style="display: block;">
    <iframe width="1280" height="720" src="https://www.youtube.com/embed/3b5zCFSmVvU" frameborder="0" allowfullscreen></iframe>
  </div>
  <a class="close-reveal-modal">&#215;</a>
</div>
