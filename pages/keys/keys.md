---
layout: page
# title: "Keys"
meta_title: "luke's keys"
subheadline: "GPG/PGP Public Keys"
permalink: "/keys/"
---
The key (and its associated subkeys) that I attempt to use for all personal communication today is [0x3725C2AF063E7467](./key-luke-3725C2AF063E7467.asc). It is a newer 4096 bit RSA key.

Key information useful in a keysigning is listed below:

    pub   rsa4096/0x3725C2AF063E7467 2015-02-09
          Key fingerprint = 9092 0B7B AABE 9423 881A  7F40 3725 C2AF 063E 7467
    uid                   Luke Swart <luke@lukeswart.net>
    uid                   Luke Swart <luke@smartercleanup.org>
    sub   rsa4096/0x1FCED9F7F31DCAAF 2015-02-09

If you'd like to find this information in a [text file](./key-fingerprints.txt), or my key in a [text file](./key-luke-3725C2AF063E7467.txt) for easy printing, there is one posted here.
