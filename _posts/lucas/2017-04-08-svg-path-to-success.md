---
layout: page
title: "SVG Paths: The Road to Better Visualizations"
subheadline: SVG
image:
    title: path-arrow.svg
    thumb: path-arrow.svg
    homepage: path-arrow.svg
    caption_url: /images/path-arrow.svg
tags:
  - frontend
categories:
  - computers
breadcrumb: true
teaser: "The SVG `path` element is well known, yet often underutilized. There is lots of information written about it, but I've seen a few misconceptions around creating custom visualizations, so I think it warrants another post"
---

In this post, we will explore an example where an SVG `path` element can be used instead of a more complicated implementation. This can apply to many cases where multiple elements are composed together when a single `path` element with do the job.

In this example, we are going to draw an arrow like the one shown in the header above.

But don't do it like this:

<div style="display: flex; width: 100%;">
  <svg width="100%" viewBox="0 0 200 60">
    <g class="example1">
      <rect fill="#0000FF"
            x="0" y="10" width="100" height="25" />
      <path fill="#0000FF"
            d="M100 0 l 30 22.5 l -30 22.5z"/>
    </g>
    <defs>
      <filter id="dropshadow" height="130%">
        <feGaussianBlur in="SourceAlpha" stdDeviation="3"/> <!-- stdDeviation is how much to blur -->
        <feOffset dx="2" dy="2" result="offsetblur"/> <!-- how much to offset -->
        <feMerge>
          <feMergeNode/> <!-- this contains the offset blurred image -->
          <feMergeNode in="SourceGraphic"/> <!-- this contains the element that the filter is applied to -->
        </feMerge>
      </filter>
    </defs>
  </svg>
</div>

Note the artifacts that we get, like the line between the triangle and the rectangle. This is because we are drawing an svg `rect`, then appending a triangle on top of it. This can be tempting when using a library like D3, which provides wrappers that easily produce rectangles, circles, lines, etc. But instead of composing multiple shapes together, I suggest taking a much simpler path.

Let's try drawing the same arrow, but using a single SVG `path` element instead:

<div style="display: flex; width: 100%;">
  <svg width="100%" viewBox="0 0 200 60">
    <g class="example1">
      <path fill="#0000FF"
            d="
            M 0 20
            l 100 0
            l 0 -10
            l 30 22
            l -30 22
            l 0 -10
            l -100 0
            z
            "/>
    </g>
    <defs>
      <filter id="dropshadow" height="130%">
        <feGaussianBlur in="SourceAlpha" stdDeviation="3"/> <!-- stdDeviation is how much to blur -->
        <feOffset dx="2" dy="2" result="offsetblur"/> <!-- how much to offset -->
        <feMerge>
          <feMergeNode/> <!-- this contains the offset blurred image -->
          <feMergeNode in="SourceGraphic"/> <!-- this contains the element that the filter is applied to -->
        </feMerge>
      </filter>
    </defs>
  </svg>
</div>

And here is the code for that arrow:

~~~ html
  <!-- use 'viewBox' to set a lengthxwidth coordinate system of 200x60 -->
  <svg width="100%" viewBox="0 0 200 60">
    <path class="path" fill-opacity="0.0" stroke="#000000" d="
          M 0 20
          l 100 0
          l 0 -10
          l 30 22
          l -30 22
          l 0 -10
          l -100 0
          z
    "/>
  </svg>
~~~

Let's explore how this path element is drawn. Here is a breakdown of how we use the "d" attribute above to draw the arrow:

`M 0 20`   -- This is our starting point in the top-left corner of the arrow
 (x=0, y=20 in our viewBox)

`l 100 0`  -- Move 100 units to the right

`l 0 -10`  -- Move 10 units up

`l 30 22`  -- Move 30 units right, 22 units down
                   (this is the tip of our arrow!)

`l -30 22` -- Move 30 units left, 22 units down

`l 0 -10`  -- Move 10 units down

`l -100 0` -- Move 100 units to the left

`z`  -- Close our arrow polygon by moving back to our starting point


And here is that same arrow, with an added animation to emphasize the trajectory of the path:

<div style="display: flex; width: 100%;">
  <svg width="100%" viewBox="0 0 200 60">
    <path class="path" fill-opacity="0.0" stroke="#0000FF" d="
      M 0 20
      l 100 0
      l 0 -10
      l 30 22
      l -30 22
      l 0 -10
      l -100 0
      z
    "/>
  </svg>
</div>


<style>
.path {
  stroke-dasharray: 700;
  stroke-dashoffset: 700;
  animation: dash 5s linear forwards infinite;
}

@keyframes dash {
  to {
    stroke-dashoffset: 0;
  }
}
</style>

And as a bonus, imagine trying to draw the animation above over multiple polygons! But for our example above, we only need to add the following styles to our `<path>` element, which has been labeled with the class name `path`:

~~~html
<style>
.path {
  stroke-dasharray: 700;
  stroke-dashoffset: 700;
  animation: dash 5s linear forwards infinite;
}

@keyframes dash {
  to {
    stroke-dashoffset: 0;
  }
}
</style>
~~~


## Conclusion
I hope this is useful to anyone new to drawing custom SVG shapes. The `path` element is very powerful in that it can create any polygon, line, or curved shape. I added some links to the SVG `path` docs and its `d` attribute below.

Further reading:

[SVG \<path\> documentation](https://developer.mozilla.org/en-US/docs/Web/SVG/Element/path)

[docs for the \<path\> element's "d" attribute](https://developer.mozilla.org/en-US/docs/Web/SVG/Attribute/d)

[SVG \<path\> tutorial](http://tutorials.jenkov.com/svg/path-element.html)
