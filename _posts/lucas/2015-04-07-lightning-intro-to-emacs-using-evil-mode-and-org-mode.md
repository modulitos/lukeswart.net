---
layout: page
title: Lightning intro to Emacs using evil-mode and org-mode
subheadline: emacs
image:
    title: org-mode-screenshot.png
    thumb: org-mode-screenshot-thumb.png
    homepage: org-mode-screenshot.png
    caption: org-mode example (click here for full view)
    caption_url: /images/org-mode-screenshot.png
tags:
  - emacs
categories:
  - computers
teaser: "Forget mind maps, clunky spreadsheets, and vendor lock-in tools that are often more annoying than helpful. Instead, let's explore org-mode - a grass-roots free-software project that helps you manage your life in plain text! An accelerated emacs intro is also included."
breadcrumb: true
---

There are a ton of good org-mode guides, and lots of evil-mode documentation, but here is a quick guide to get up and running with a vim-emulating editor. We'll even apply those text editing abilities to channel the organizational power of Emacs' org-mode.

First, we'll install evil mode. These install instructions were taken from the <a href="http://www.emacswiki.org/emacs/Evil">Emacs wiki</a>:

Install using the latest version of Emacs and its builtin package system. Start by adding this in your '~/.emacs':

~~~ elisp
(require 'package)
(push '("marmalade" . "http://marmalade-repo.org/packages/")
package-archives )
(push '("melpa" . "http://melpa.milkbox.net/packages/")
package-archives)
~~~

Then execute the following commands in your Emacs minibuffer, located at the bottom of the window:

~~~
M-x list-packages
C-s evil
~~~

Use `C-s` to select the Evil package and press enter. Follow the prompt to install. Note that `M` stands for meta, which is the `alt` key. `C` means the `ctrl` key.

The latest stable version of Evil-mode is available from the Marmalade repository, which you have just fetched and installed.

After installation, add the following to your `~/.emacs` config file. Alternatively, you can add the code into `~/.emacs.d/init.el` and delete `~/.emacs` if you want a more scalable config:

Feel free to download the file here:

<a href="https://gist.github.com/LukeSwart/32abfe6a11943fcf69af">emacs-test-init-file</a>

Here is the Elisp code (it will look prettier in your buffer!):

~~~ elisp
;; Basic evil-mode and org-mode config to get started
;; Include this in your Emacs config file (ie ~/.emacs.d/init.el or ~/.emacs)
(require 'evil)
;; Enable Evil mode as defuault
(evil-mode 1)
;; Treat wrapped line scrolling as single lines
(define-key evil-normal-state-map (kbd "j") 'evil-next-visual-line)
(define-key evil-normal-state-map (kbd "k") 'evil-previous-visual-line)
;;; esc quits pretty much anything (like pending prompts in the minibuffer)
(define-key evil-normal-state-map [escape] 'keyboard-quit)
(define-key evil-visual-state-map [escape] 'keyboard-quit)
(define-key minibuffer-local-map [escape] 'minibuffer-keyboard-quit)
(define-key minibuffer-local-ns-map [escape] 'minibuffer-keyboard-quit)
(define-key minibuffer-local-completion-map [escape] 'minibuffer-keyboard-quit)
(define-key minibuffer-local-must-match-map [escape] 'minibuffer-keyboard-quit)
(define-key minibuffer-local-isearch-map [escape] 'minibuffer-keyboard-quit)
;; Enable smash escape (ie 'jk' and 'kj' quickly to exit insert mode)
(define-key evil-insert-state-map "k" #'cofi/maybe-exit-kj)
(evil-define-command cofi/maybe-exit-kj ()
:repeat change
(interactive)
(let ((modified (buffer-modified-p)))
(insert "k")
(let ((evt (read-event (format "Insert %c to exit insert state" ?j)
nil 0.5)))
(cond
((null evt) (message ""))
((and (integerp evt) (char-equal evt ?j))
(delete-char -1)
(set-buffer-modified-p modified)
(push 'escape unread-command-events))
(t (setq unread-command-events (append unread-command-events
(list evt))))))))
(define-key evil-insert-state-map "j" #'cofi/maybe-exit-jk)
(evil-define-command cofi/maybe-exit-jk ()
:repeat change
(interactive)
(let ((modified (buffer-modified-p)))
(insert "j")
(let ((evt (read-event (format "Insert %c to exit insert state" ?k)
nil 0.5)))
(cond
((null evt) (message ""))
((and (integerp evt) (char-equal evt ?k))
(delete-char -1)
(set-buffer-modified-p modified)
(push 'escape unread-command-events))
(t (setq unread-command-events (append unread-command-events
(list evt))))))))
;; ORG MODE
;; auto-indent an org-mode file
(add-hook 'org-mode-hook
(lambda()
(local-set-key (kbd "C-c C-c") 'org-table-align)
(local-set-key (kbd "C-c C-f") 'org-table-calc-current-TBLFM)
(org-indent-mode t)))
~~~

To refresh your config, have your config file open and run `M-x eval-buffer`. Or just restart emacs (save in Emacs via `C-x C-s` and close via `C-x C-c`)

You can toggle evil and org modes with `M-x org-mode` and `M-x evil-mode` respectively.

After that, create any `.org` file and you should have a vim-emulated editor in org-mode!

Let's create a sample org-file to try out our evil-mode editing on an org-mode file. To test an example org-file, execute `C-x C-f`, type in a filepath, ie `~/test/sample-org-file.org`, and add these contents to it:

<a href="https://gist.github.com/LukeSwart/0c880aa1dd56bc1bb26d">beginner's-org-file</a>

Feel free to download the file above and copy/paste it into your new `.org` file.

You should now be feeling the power of Vim and Org-mode! You might even experience why they call it `evil-mode` :-)

For further tips and tricks, feel free to check out the <a href="http://orgmode.org/worg/org-tutorials/org4beginners.html">org mode docs</a>. Just remember, the applications with org-mode are extremely expansive. For example, if you are inclined, you can even use it to <a href="http://emacslife.com/emacs-chats/chat-tom-marble.html">log your hours and invoice clients from a latex template!</a>

Have fun!
