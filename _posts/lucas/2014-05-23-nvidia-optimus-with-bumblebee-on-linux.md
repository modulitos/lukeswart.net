---
layout: page
title: Nvidia Optimus with Bumblebee on Ubuntu 14.04
subheadline: desktop drivers
tags:
  - linux
categories:
  - computers
teaser: "Troubleshooting an all too common issue"
breadcrumb: true
---

This post is a guide to enable Bumblebee with an external monitor on Ubuntu 14.04 for a ThinkPad-W520. This may work for W-500's, T-400's, and other Optimus-enabled machines with <a href="https://wiki.archlinux.org/index.php/Bumblebee#Multiple_monitors">outputs wired to the NVIDIA chip</a>.

I am tailoring this article for the novice Linux user who may be struggling to enable the external monitor or interested in achieving the power savings that a switchable graphics technology provides in Ubuntu 14.04. More advanced users may opt to configure their setup directly using <a href="https://github.com/Bumblebee-Project/Bumblebee/wiki/Multi-monitor-setup">Bumblebee's wiki page</a> (I encourage all users to read this page for background on the project).

In short, if you have the following setup:

<ul>
<li>ThinkPad-W520</li>
<li>External monitor</li>
<li>Ubuntu 14.04</li>
</ul>

Then this guide is here to help!

h3. First, install the appropriate Bumblebee and Nvidia dependencies



(big thanks to <a href="http://askubuntu.com/questions/452556/how-to-set-up-nvidia-optimus-bumblebee-in-14-04">Pavak Paul</a>):

<pre>@sudo apt-get install bumblebee bumblebee-nvidia primus nvidia-331
@</pre>

14.04 offers a great GUI that I recommend as well:

<pre>@sudo apt-get install python-appindicator
@</pre>

In your "@~@" directory, do the following:

<pre>@mkdir git && cd git
@</pre>

Check out and install the repository:

<pre>@git clone https://github.com/Bumblebee-Project/bumblebee-ui.git
cd bumblebee-ui
sudo ./INSTALL
@</pre>

Go to "@Startup Applications@" and add "@bumblebee-indicator@"

If you haven't already, you may need to install the following:

<pre>@sudo add-apt-repository ppa:xorg-edgers/ppa
sudo apt-get update
@</pre>

Now reboot. After reboot, if the indicator is in your startup settings, you should have the following Bumblebee menu (circled in red, second from the left):
<a href="http://blog.lukeswart.net/wordpress/wp-content/uploads/2014/05/bumblebee_menu.png"><img src="http://blog.lukeswart.net/wordpress/wp-content/uploads/2014/05/bumblebee_menu-300x49.png" alt="bumblebee_menu_highlighted" width="300" height="49" class="alignnone size-medium wp-image-39" /></a>

h3. Next, configure Bumblebee



(HUGE thanks to <a href="http://www.unixreich.com/blog/2013/linux-nvidia-optimus-on-thinkpad-w520w530-with-external-monitor-finally-solved/">Scyth</a>, from which this excerpt is derived):

You’ll need to edit "@/etc/bumblebee/bumblebee.conf@" and find and change these params, so they look like:

<pre>@KeepUnusedXServer=true
Driver=nvidia
KernelDriver=nvidia-331
PMMethod=none (find this one in two locations in the file)
@</pre>

Next, edit "@/etc/bumblebee/xorg.conf.nvidia@" and make it look like this:

<pre>@Section "ServerLayout"
Identifier "Layout0"
EndSection

Section "Device"
Identifier "DiscreteNvidia"
Driver "nvidia"
VendorName "NVIDIA Corporation"
BusID "PCI:01:00:0"
Option "ProbeAllGpus" "false"
Option "NoLogo" "true"
EndSection
@</pre>

h3. Next, configure your X11 server



(thanks to Bumblebee's <a href="https://github.com/Bumblebee-Project/Bumblebee/wiki/Multi-monitor-setup">wiki page</a>):

Add the following to "@/etc/X11/xorg.conf@". Note that this may not be necessary for many, but it cannot hurt. For more understanding, read up on the wiki page referenced above.

<pre>@Section "ServerLayout"
Identifier "Layout0"
EndSection
Section "Device"
Identifier "Device1"
Driver "nvidia"
VendorName "NVIDIA Corporation"
Option "NoLogo" "true"
Option "ConnectedMonitor" "DFP"
EndSection
#For the configuration with bumblebee installed
#Section that follows come from archlinux adapt it from your distro if necessary.
Section "Files"
ModulePath "/usr/lib/nvidia/xorg/"
ModulePath "/usr/lib/xorg/modules/"
EndSection
@</pre>

h3. Setup the "@intel-virtual-output@" driver tool:



Download the source here: <a href="https://01.org/linuxgraphics/downloads">https://01.org/linuxgraphics/downloads</a> and choose the latest "@xf86-video-intel@". At the time of this post, version was 2.99.910

Extract it:

<pre>@tar -xvf xf86-video-intel-2.99.910.tar.gz
@</pre>

Then build it (more info <a href="http://www.linuxquestions.org/questions/linux-newbie-8/question-how-to-install-intel-drivers-912512/">here</a>):

<pre>@cd xf86-video-intel-2.99.910/
./configure
make
sudo make install
@</pre>

This should provide you with the command "@intel-virtual-output@". Double check that this command is available to you in your prompt (via tab-completion):

<pre>@intel-virtual-output
@</pre>

h3. The moment of truth



(again, lots of help from <a href="http://www.unixreich.com/blog/2013/linux-nvidia-optimus-on-thinkpad-w520w530-with-external-monitor-finally-solved/">Scythe</a> on this):

<pre>@modprobe bbswitch
optirun true
intel-virtual-output
@</pre>

<pre>@# kill the second X server.
# To find the process, run: ps ax | grep Xorg
# You should see something like this
$ ps ax | grep Xorg
3342 ? Ss 68:08 Xorg :8 -config /etc/bumblebee/xorg.conf.nvidia -configdir /etc/bumblebee/xorg.conf.d -sharevts -nolisten tcp -noreset -verbose 3 -isolateDevice PCI:01:00:0 -modulepath /usr/lib/nvidia-331/xorg,/usr/lib/xorg/modules
# now kill the process
$ sudo kill -15 3342
@</pre>

<pre>@# Now you need to turn off nvidia card completely.
sudo rmmod nvidia
sudo tee /proc/acpi/bbswitch <<<OFF
@</pre>

h1. Automate multi-screen switching on-the-go:



To automate this process, I created the following simple scripts in my "@~/bin/@":

<ul>
<li>start the multi-screen: <a href="https://gist.github.com/LukeSwart/060343217da018016961">https://gist.github.com/LukeSwart/060343217da018016961</a></li>
<li>stop the multi-screen: <a href="https://gist.github.com/LukeSwart/eb18ec7099ea3f3557a3">https://gist.github.com/LukeSwart/eb18ec7099ea3f3557a3</a></li>
<li>restart the multi-screen: <a href="https://gist.github.com/LukeSwart/ce89005d8c112bbf9ba2">https://gist.github.com/LukeSwart/ce89005d8c112bbf9ba2</a></li>
</ul>

You can add the following aliases as shown here: <a href="https://gist.github.com/LukeSwart/b9e8560b1374592a5286">https://gist.github.com/LukeSwart/b9e8560b1374592a5286</a>

Here are some inspiring scripts by <a href="https://gist.github.com/cardil/10533170">Krzysztof Suszyski</a> which also automate the graphics switching as well as the installation. I would NOT recommend installing it verbatim since it is tailored for Ubuntu 13.10 and the configuration may different depending on the machine.

Any questions, I will do my best to help out. Thank you!

h3. Here are some helpful sources:



<ul>
<li><a href="http://www.unixreich.com/blog/2013/linux-nvidia-optimus-on-thinkpad-w520w530-with-external-monitor-finally-solved/">http://www.unixreich.com/blog/2013/linux-nvidia-optimus-on-thinkpad-w520w530-with-external-monitor-finally-solved/</a></li>
<li><a href="https://github.com/Bumblebee-Project/Bumblebee/wiki/Multi-monitor-setup">https://github.com/Bumblebee-Project/Bumblebee/wiki/Multi-monitor-setup</a></li>
<li><a href="http://steamcommunity.com/app/221410/discussions/5/810939351078447225/">http://steamcommunity.com/app/221410/discussions/5/810939351078447225/</a></li>
</ul>

This post is designed as a guided walk-through, and I encourage the reader to understand each step along the way. Feel free to contact me directly with any issues or suggestions.
